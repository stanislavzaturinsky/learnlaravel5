@extends('app')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Show post</h2>
            </div>
            <div class="pull-right">
                <a href="{{ route('post.edit', $post->id) }}" class="btn btn-primary">Edit</a>
                {!! Form::open(['method' => 'DELETE', 'route' => ['post.destroy', $post->id], 'style' => 'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
                <a href="{{ route('post.index') }}" class="btn btn-info"> Back</a>
            </div>
        </div>
    </div>

    <table class="table table-bordered">
        <tr>
            <td><strong>Title:</strong></td>
            <td>{{ $post->title }}</td>
        </tr>
        <tr>
            <td><strong>Slug:</strong></td>
            <td>{{ $post->slug }}</td>
        </tr>
        <tr>
            <td><strong>Excerpt:</strong></td>
            <td>{{ $post->excerpt }}</td>
        </tr>
        <tr>
            <td><strong>Content:</strong></td>
            <td>{{ $post->content }}</td>
        </tr>
        <tr>
            <td><strong>Published:</strong></td>
            <td>{{ $post->published ? 'on' : 'off' }}</td>
        </tr>
        <tr>
            <td><strong>Published at:</strong></td>
            <td>{{ $post->published_at }}</td>
        </tr>
    </table>
@stop