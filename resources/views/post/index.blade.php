@extends('app')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>{{ $title }}</h2>
            </div>
            <div class="pull-right">
                <a href="{{ route('post.create') }}" class="btn btn-success">Create new post</a>
            </div>
        </div>
    </div>

    <table class="table table-bordered">
        <tr>
            <th>№</th>
            <th>ID</th>
            <th>Title</th>
            <th>Slug</th>
            <th>Published at</th>
            <th width="20%">Action</th>
        </tr>
        @foreach($posts as $key => $post)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $post->id }}</td>
                <td>{{ $post->title }}</td>
                <td>{{ $post->slug }}</td>
                <td>{{ $post->published_at }}</td>
                <td>
                    <a href="{{ route('post.show', $post->id) }}" class="btn btn-info">Show</a>
                    <a href="{{ route('post.edit', $post->id) }}" class="btn btn-primary">Edit</a>
                    {!! Form::open(['method' => 'DELETE', 'route' => ['post.destroy', $post->id], 'style' => 'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>
@stop