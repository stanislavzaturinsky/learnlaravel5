@extends('app')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <a href="{{ route('post.index') }}" class="btn btn-info"> Back</a>
        </div>
    </div>

    <h1>Edit</h1>
    {!! Form::model($post, ['method' => 'put', 'route' => ['post.update', $post->id]]) !!}
        {!! Form::hidden('post_id', $post->id) !!}
        @include('post._form')
    {!! Form::close() !!}

@stop