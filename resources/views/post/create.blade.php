@extends('app')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <a href="{{ route('post.index') }}" class="btn btn-info"> Back</a>
        </div>
    </div>

    <h1>Create</h1>
    {!! Form::open(['route' => 'post.store']) !!}
        @include('post._form')
    {!! Form::close() !!}

@stop