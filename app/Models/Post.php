<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model {

    protected $fillable = ['title', 'slug', 'excerpt', 'content', 'published', 'published_at'];

    public function getPublishedPosts() {
        return self::latest('published_at')->published()->get();
    }

    public function scopePublished($query) {
//        $query->where('published_at', '<=', Carbon::now())
        $query->where('published', '=', 1);
    }

    public function getUnPublishedPosts() {
        return self::latest('published_at')->unPublished()->get();
    }

    public function scopeUnPublished($query) {
//        $query->where('published_at', '=>', Carbon::now())
        $query->where('published', '=', 0);
    }
}
