<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Session;
use App\Models\Post;

class PostController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Post $postModel)
	{
        $posts = $postModel->getPublishedPosts();
        $title = 'Published posts';
        return view('post.index', ['posts' => $posts, 'title' => $title]);
	}

	public function unpublished(Post $postModel) {
	    $posts = $postModel->getUnPublishedPosts();
        $title = 'Unpublished posts';
        return view('post.index', ['posts' => $posts, 'title' => $title]);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('post.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Post $postModel, Request $request)
	{
	    $this->validate($request, self::rules($request), self::message());

		if($postModel->create($request->all())) {
            $request->session()->flash('alert-success', 'Post created successfully');
        } else {
            $request->session()->flash('alert-danger', 'Post not saved');
        }
		return redirect()->route('posts');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$postModel = Post::find($id);
		return view('post.show', ['post' => $postModel]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $postModel = Post::find($id);
        return view('post.edit', ['post' => $postModel]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
        $this->validate($request, self::rules($request), self::message());

		if(Post::find($id)->update($request->all())) {
		    $request->session()->flash('alert-success', 'Post updated successfully');
        } else {
            $request->session()->flash('alert-danger', 'Post not updated');
        }
		return redirect()->route('post.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id)
	{
	    $referrer = $request->headers->get('referer');
		Post::find($id)->delete();
        $request->session()->flash('alert-success', 'Post deleted successfully');
        if(strpos($referrer, 'unpublished')) {
            return redirect()->route('posts.unpublished');
        } else {
            return redirect()->route('posts');
        }
	}

	public function rules(Request $request) {
	    if($request->method() == 'PUT') {
            $slug_rule = [
                'required',
                'unique:posts,slug,' . $request->get('post_id')
            ];
        } else {
	        $slug_rule = [
                'required',
                'unique:posts,slug'
            ];
        }

        return [
            'title' => [
                'required',
                'between:3,30',
            ],
            'slug' => $slug_rule,
            'excerpt' => [
                'required',
                'max:200'
            ],
            'content'      => 'required',
            'published'    => 'in:0,1',
            'published_at' => 'required',
        ];
    }

	public function message() {
	    return [
	        'required' => ':attribute is required!',
	        'between'  => ':attribute must between :min and :max!',
            'in'       => ':attribute must contain one value from: :values',
            'unique'   => ':attribute already exist. Field must be unique',
            'max'      => ':attribute must be not more than :max symbols'
        ];
    }
}
