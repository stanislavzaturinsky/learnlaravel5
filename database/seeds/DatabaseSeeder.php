<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Post;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

//		$this->call('UsersSeeder');
//      $this->call('PostsSeeder');
	}

}

class PostsSeeder extends Seeder {
    public function run() {
        DB::table('Posts')->delete();
        Post::create([
            'title'        => 'First Post',
            'slug'         => 'first-post',
            'excerpt'      => '<b>First Post Body</b>',
            'content'      => '<b>Content first post body</b>',
            'published'    => true,
            'published_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);

        Post::create([
            'title'        => 'Second Post',
            'slug'         => 'second-post',
            'excerpt'      => '<b>Second Post Body</b>',
            'content'      => '<b>Content second post body</b>',
            'published'    => false,
            'published_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);

        Post::create([
            'title'        => 'Third Post',
            'slug'         => 'third-post',
            'excerpt'      => '<b>Third Post Body</b>',
            'content'      => '<b>Content Third post body</b>',
            'published'    => true,
            'published_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
    }
}

class UsersSeeder extends Seeder {
    public function run() {
        DB::table('users')->delete();
        User::create([
            'name'     => 'Petr Ivanov',
            'email'    => 'ivanov@gmail.com',
            'password' => Hash::make('123123'),
        ]);

        User::create([
            'name'     => 'Alexander Makedonsky',
            'email'    => 'makedonsky@gmail.com',
            'password' => Hash::make('qwerty'),
        ]);
    }
}
